###### Temat projektu:
### Katalog zdjęć

###### Opis projektu:
Użytkownik ma możliwość edycji atrybutów EXIF zdjęcia. Na podstawie informacji zawartych w poszczególnych sekcjach EXIF, użytkownik ma możliwość filtrowania i sortowania zdjęć. Filtrowanie i sortowanie zdjęć może odbywać się za pomocą np. długości ogniskowej obiektywu, którym zrobiono zdjęcie, czasu migawki, typu aparatu itp. Program udostępnia graficzny interfejs użytkownika, umożliwiający podgląd zdjęć.